## create：
- 左右移动
  - hspeed_ = 0
  - walk_speed = 8

- 跳跃
  - jump_speed = 15

- 下落
  - vspeed_ = 0
  - gravity_ = 0.7

- 多段跳
  - jump_stage = 0
  - max_jump_stage = 2

- 地面判断
  - is_on_floor = false

- 减速
  - acc = 0
  - acc_walk = 0.8
  - acc_jump = 0.3

normal_speed = 0

is_dead = false
is_show_gameover = false

state = "idle"
face_towards = 1



## step:
### 变量和方法定义
#### 1. 变量定义
- 方向键定义
  - left
  - right
  - left_pressed
  - right_pressed
  - up_pressed

- 移动方向
  - move_direction = right - left


#### 2. 方法定义
- 落地判断
- 碰撞检测


### 状态机:条件判断，state
1. idle
   1. 状态逻辑
   2. 状态切换
      1. ->walk
         1. left_right_pressed
      2. ->run
      3. ->jump
         1. up_pressed
      4. ->fall
2. walk
   1. 状态逻辑
      1. 左右移动
         1. 减速
   2. 状态切换
      1. ->idle
         1. 速度绝对值 < 0.1
      2. ->run
      3. ->jump
         1. up_pressed
            1. 多段跳判定
      4. ->fall
3. run

4. jump

5. fall



### 不理解/可优化的点
- 二段跳代码

