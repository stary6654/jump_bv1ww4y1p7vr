// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

//==================================================================
//@function
//==================================================================
function collide_horizontal() {
    if place_meeting(x+hspeed_, y, obj_floor) {
        //normal_speed = vspeed_ / abs(vspeed_)
        normal_speed = sign(hspeed_)
        while not place_meeting(x+normal_speed, y, obj_floor) {
            x += normal_speed
        }
        hspeed_ = 0
    }
}


//==================================================================
//@function
//==================================================================
function collide_vertical() {
    if place_meeting(x, y+vspeed_, obj_floor) {
        //normal_speed = vspeed_ / abs(vspeed_)
        normal_speed = sign(vspeed_)
        while not place_meeting(x, y+normal_speed, obj_floor) {
            y += normal_speed
        }
        vspeed_ = 0
    }
}


//==================================================================
//@function
//==================================================================
function is_on_floor() {
    if place_meeting(x, y+1, obj_floor) {
        return true
    } else {
        return false
    }
}