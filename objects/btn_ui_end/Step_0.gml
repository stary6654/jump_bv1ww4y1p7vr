/// @description Insert description here
// You can write your code in this editor

if mouse_over {
	END_SIGN.visible = true
	
	if mouse_check_button_pressed(mb_left) {
		game_end()
	}
	
	if scale < 1.2 {
		scale += 0.1
		image_xscale = scale
		image_yscale = scale
	}
} else {
	END_SIGN.visible = false
	
	if scale > 1 {
		scale -= 0.1
		image_xscale = scale
		image_yscale = scale	
	}
}