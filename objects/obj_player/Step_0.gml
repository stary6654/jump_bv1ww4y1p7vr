/// @description Insert description here

#region 输入映射=====================================================================
                 	
left = keyboard_check(vk_left) or keyboard_check(ord("A"))
right = keyboard_check(vk_right) or keyboard_check(ord("D"))

left_pressed = keyboard_check_pressed(vk_left) or keyboard_check_pressed(ord("A"))
right_pressed = keyboard_check_pressed(vk_right) or keyboard_check_pressed(ord("D"))
up_pressed = keyboard_check_pressed(vk_up) or keyboard_check_pressed(ord("W"))
any_pressed = keyboard_check_pressed(vk_anykey) or mouse_check_button_pressed(mb_any)

move_direction = right - left

#endregion


#region 状态之外========================================================================

// 到达终点
if place_meeting(x, y, obj_goal) and state != "dead" {
    if keyboard_check_pressed(ord("E")) {
        room_goto(rm_level2)
    }
}

#endregion


#region 行为树：进入状态机================================================================

#endregion


#region 状态机============================================================================

if state == "idle" { //------------------------------------------------------------------
    // 状态逻辑 ------------------------------
    jump_stage = 0
    // 动画设置
    sprite_index = player_idle

    // 状态跳转-------------------------------
    // -> walk
	if left or right state = "walk"

    // -> jump
    if up_pressed {
        if jump_stage < max_jump_stage {
            state = "jump"
            vspeed_ = -jump_speed
            jump_stage++
        }
    }

    // -> fall
    if not is_on_floor() state = "fall"

} else if state == "walk" { //---------------------------------------------------------
    // 状态逻辑--------------------------------

    // 水平朝向
    if left face_towards = -1
    if right face_towards = 1

    // 动画设置
    sprite_index = player_walk
    image_xscale = face_towards

    // 水平移动
    if left or right {
        hspeed_ = face_towards * walk_speed
    } else {
        // 提前终止动画
        sprite_index = player_idle
        // 插值：缓慢停止
        hspeed_ = lerp(0, hspeed_, 0.8)
    }

    // 水平碰撞检测
    collide_horizontal()
	
    // 应用于坐标
	x += hspeed_
	
    // 状态跳转-------------------------------
    // -> idle
    if abs(hspeed_) < 0.1 state = "idle"

    // -> jump
    if up_pressed {
        if jump_stage < max_jump_stage {
            state = "jump"
            vspeed_ = -jump_speed
            jump_stage++
        }
    }

    // -> fall
    if not is_on_floor() state = "fall"
} else if state == "run" { //------------------------------------------------------------
    // 状态跳转
} else if state == "jump" { //-----------------------------------------------------------
    // 状态逻辑------------------------------
    // 动画设置
    sprite_index = player_jump

    // 多段跳
    if up_pressed {
        if jump_stage < max_jump_stage {
            state = "jump"
            vspeed_ = -jump_speed
            jump_stage++
        }
    }

    // 空中左右移动
    if left face_towards = -1
    if right face_towards = 1
    if left or right {
        hspeed_ = face_towards * walk_speed
    } else {
        // 插值
        hspeed_ = lerp(0, hspeed_, 0.95)
    }
    
    // 碰撞检测
    collide_horizontal()
    collide_vertical()

    // 应用到坐标
    x += hspeed_
    y += vspeed_
    vspeed_ += gravity_

    // 状态切换----------------------------------
    // -> fall
    if vspeed_ > 0 state = "fall"
    
} else if state == "fall" { //-----------------------------------------------------------
    // 状态逻辑-------------------------------

    // 动画设置
    sprite_index = player_fall

    // 跳跃，多段跳
    if up_pressed {
        if jump_stage < max_jump_stage {
            vspeed_ = -jump_speed
            jump_stage++
        }
    }
    
    // 空中左右移动
    if left face_towards = -1
    if right face_towards = 1
    if left or right {
        hspeed_ = face_towards * walk_speed
    } else {
        // 插值
        hspeed_ = lerp(0, hspeed_, 0.95)
    }

    // 碰撞检测
    collide_horizontal()
    collide_vertical()

    // 应用到坐标
    x += hspeed_
    y += vspeed_
    vspeed_ += gravity_

    // 状态切换-------------------------------
    if is_on_floor() state = "idle"
} else if state == "dead" { //-----------------------------------------------------------
    // 状态逻辑----------------------------
	image_alpha -= 0.025
	if image_alpha < -0.25 {
		//room_restart()
		is_show_gameover = true
		if any_pressed {
			room_restart()
		}
	}
} 


#endregion


#region 测试用===========================================================================

if keyboard_check_pressed(vk_f5) room_restart()

exit

#endregion
