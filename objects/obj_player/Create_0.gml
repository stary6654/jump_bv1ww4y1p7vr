/// @description Insert description here

// 左右移动
hspeed_ = 0
face_towards = 1
walk_speed = 5

// 垂直速度，跳跃
vspeed_ = 0
jump_speed = 12

// fall
gravity_ = 0.7

// 速度方向单位向量
normal_speed = 0

// 多段跳
jump_stage = 0
max_jump_stage = 2

// 地面检测
is_on_floor = false

// 状态
state = "idle"

// UI
is_show_gameover = false


