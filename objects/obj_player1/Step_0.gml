/// @description Insert description here
// You can write your code in this editor


// 左右移动
if keyboard_check(ord("D")) or keyboard_check(vk_right) {
    hspeed += accel;
    // image_xscale = 1;
} else if keyboard_check(ord("A")) or keyboard_check(vk_left) {
    hspeed -= accel;
    // image_xscale = -1;
} else {
    hspeed *= friction;
}

// 跳跃
if place_free(x, y+1) and (keyboard_check(ord("W")) or keyboard_check(vk_up)) {
    vspeed = jump_strength;
}

// 应用速度和重力
x += hspeed;
y += vspeed;
vspeed += gravity;

// 碰撞检测
if collision_point(x, y+1, obj_floor, false, true) {
    y--;
    vspeed = 0;
}