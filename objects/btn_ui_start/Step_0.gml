/// @description Insert description here
// You can write your code in this editor

if mouse_over {
	//inst_75D9437D.visible = true
	START_SIGN.visible = true
	
	if mouse_check_button_pressed(mb_left) {
		room_goto(rm_level1)
	}
	
	if scale < 1.2 {
		scale += 0.1
		image_xscale = scale
		image_yscale = scale
	}
} else {
	START_SIGN.visible = false
	
	if scale > 1 {
		scale -= 0.1
		image_xscale = scale
		image_yscale = scale	
	}
}
